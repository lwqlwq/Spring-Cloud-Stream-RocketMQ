package com.erbadagang.springcloudalibaba.stream.rocketmq.consumer.listener;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.messaging.SubscribableChannel;

public interface MySink {

    String ERBADAGANG_INPUT = "erbadagang-input";

    String ERBADAGANG_INPUT_2 = "erbadagang-input_2";

    @Input(ERBADAGANG_INPUT) SubscribableChannel demo01Input();

    @Input(ERBADAGANG_INPUT_2) SubscribableChannel demo01Input1();

    String TREK_INPUT = "trek-input";

    @Input(TREK_INPUT) SubscribableChannel trekInput();

}
