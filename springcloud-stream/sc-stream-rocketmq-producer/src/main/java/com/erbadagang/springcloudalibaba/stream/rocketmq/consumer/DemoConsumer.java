package com.erbadagang.springcloudalibaba.stream.rocketmq.consumer;

import com.erbadagang.springcloudalibaba.stream.rocketmq.producer.message.Demo01Message;
import com.erbadagang.springcloudalibaba.stream.rocketmq.producer.message.IDUtil;
import com.erbadagang.springcloudalibaba.stream.rocketmq.producer.message.MySource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

@Component
public class DemoConsumer {

    private Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private MySource mySource;

//    @StreamListener(MySource.ERBADAGANG_INPUT)
//    public void onMessage(@Payload Demo01Message message) {
//        logger.info("[onMessage][线程编号:{} 消息被消费了：{}]", Thread.currentThread().getId(), message);
//    }


    @StreamListener(MySource.FROM_MOBILE)
    public void onMessage(Map<String, Object> msgMap) {
        logger.info("渠道端接收来自移动审批的mq消息=====================：" + msgMap.toString());
        //移动端传来的部分数据
        String prevTrackId = (String) msgMap.get("traceId");
        String channel = (String) msgMap.get("channel");
        String taskId = (String) msgMap.get("taskId");

        //渠道端自己的数据
        String hasCommand = "0";
        String allowApproveOperate = "1";
        String commandResult = "1";

        Map<String, Object> channelmsgMap = new HashMap<>(16);
        channelmsgMap.put("traceId", IDUtil.getSecureRandomId());
        channelmsgMap.put("prevTraceId", prevTrackId);
        channelmsgMap.put("requestTime", LocalDateTime.now());
        channelmsgMap.put("channel", channel);
        channelmsgMap.put("commandName", "测试");
        channelmsgMap.put("reason", "渠道端同意");
        channelmsgMap.put("taskId", taskId);
        channelmsgMap.put("hasCommand", "1".equals(hasCommand));
        channelmsgMap.put("allowApproveOperate", "1".equals(allowApproveOperate));
        channelmsgMap.put("commandResult", "1".equals(commandResult));

        Map<String, Object> headers = new HashMap<>(2);
        headers.put("TAGS", "in");
        MessageHeaders messageHeaders = new MessageHeaders(headers);
        Message<Map<String, Object>> message = MessageBuilder.createMessage(channelmsgMap, messageHeaders);
        logger.info("渠道端向移动审批发送消息============================：" + channelmsgMap.toString());
        mySource.channelSendMessage().send(message);
    }

}
