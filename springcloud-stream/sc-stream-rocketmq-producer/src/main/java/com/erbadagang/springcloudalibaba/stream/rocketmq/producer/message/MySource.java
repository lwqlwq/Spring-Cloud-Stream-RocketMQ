package com.erbadagang.springcloudalibaba.stream.rocketmq.producer.message;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.SubscribableChannel;

public interface MySource {

    @Output("erbadagang-output")
    MessageChannel erbadagangOutput();

    @Output("erbadagang-output_2")
    MessageChannel erbadagangOutput2();

    @Output("trek-output")
    MessageChannel trekOutput();

    @Output("blxd_to_jkztydsp_qdspjgxx")
    MessageChannel channelSendMessage();

    @Output("mobile_to_channel")
    MessageChannel sendMessageToChannel();



    String FROM_MOBILE = "from_Mobile";
    @Input(FROM_MOBILE)
    SubscribableChannel fromMobile();


//    String ERBADAGANG_INPUT = "erbadagang-input";
//    @Input(ERBADAGANG_INPUT)
//    SubscribableChannel demo01Input();

}
