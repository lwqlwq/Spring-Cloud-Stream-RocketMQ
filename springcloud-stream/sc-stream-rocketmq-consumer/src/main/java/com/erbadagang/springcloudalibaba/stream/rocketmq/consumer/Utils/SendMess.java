package com.erbadagang.springcloudalibaba.stream.rocketmq.consumer.Utils;

import org.apache.rocketmq.acl.common.AclClientRPCHook;
import org.apache.rocketmq.acl.common.SessionCredentials;
import org.apache.rocketmq.client.exception.MQBrokerException;
import org.apache.rocketmq.client.exception.MQClientException;
import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.client.producer.SendStatus;
import org.apache.rocketmq.common.message.Message;
import org.apache.rocketmq.remoting.RPCHook;
import org.apache.rocketmq.remoting.exception.RemotingException;

/**
 * @author liwq
 * @description
 * @date 2021/9/8 13:48
 */
public class SendMess {

    //生产
    public static void main(String[] args)
        throws MQClientException, MQBrokerException, RemotingException, InterruptedException {
        sendMess();
    }

    public static void sendMess() throws MQClientException, MQBrokerException, RemotingException, InterruptedException {
        //1.创建消息生产者producer，并制定生产者组名
        DefaultMQProducer producer = new DefaultMQProducer("123123", getAclRPCHook());
        //2.指定Nameserver地址
        // producer.setNamesrvAddr("10.199.47.152:9876;10.199.47.153:9876");
        producer.setNamesrvAddr("127.0.0.1:9876");
        //3.启动producer
        producer.start();
        for (int i = 0; i < 1; i++) {
            //4.创建消息对象，指定主题Topic、Tag和消息体
            /**
             * 参数一：消息主题Topic
             * 参数二：消息Tag
             * 参数三：消息内容
             */
            Message msg = new Message("jkztfk-to-blxd-blch", "in", ("Hello World" + i).getBytes());
            //5.发送消息
            SendResult result = producer.send(msg);
            //发送状态
            SendStatus status = result.getSendStatus();
            System.out.println("发送结果:" + result);
        }
        //6.关闭生产者producer
        producer.shutdown();
    }

    static RPCHook getAclRPCHook() {
        //测试
        return new AclClientRPCHook(new SessionCredentials("contract", "12345678"));
        //生产
        //return new AclClientRPCHook(new SessionCredentials("contract","02a3f0772"));
    }
}
