package com.erbadagang.springcloudalibaba.stream.rocketmq.consumer.listener;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.messaging.SubscribableChannel;

public interface MySink {

    String ERBADAGANG_INPUT = "erbadagang-input";
    String TREK_INPUT = "trek-input";
    String TREK_INPUT_RISK_MANAGE = "riskmanage";
    String jkztfk_to_blxd_khsjfx = "jkztfk-to-blxd-khsjfx";

    @Input(ERBADAGANG_INPUT) SubscribableChannel demo01Input();

    @Input(TREK_INPUT) SubscribableChannel trekInput();

    @Input(TREK_INPUT_RISK_MANAGE) SubscribableChannel trekInputRiskManage();

    @Input(jkztfk_to_blxd_khsjfx) SubscribableChannel quanqiujieyunkehufenxi();

}
