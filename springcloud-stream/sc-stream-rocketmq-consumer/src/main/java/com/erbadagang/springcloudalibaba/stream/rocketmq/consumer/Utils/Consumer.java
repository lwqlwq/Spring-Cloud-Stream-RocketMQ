package com.erbadagang.springcloudalibaba.stream.rocketmq.consumer.Utils;

import java.util.List;

import org.apache.rocketmq.acl.common.AclClientRPCHook;
import org.apache.rocketmq.acl.common.SessionCredentials;
import org.apache.rocketmq.client.consumer.DefaultMQPushConsumer;
import org.apache.rocketmq.client.consumer.listener.ConsumeConcurrentlyContext;
import org.apache.rocketmq.client.consumer.listener.ConsumeConcurrentlyStatus;
import org.apache.rocketmq.client.consumer.listener.MessageListenerConcurrently;
import org.apache.rocketmq.client.consumer.rebalance.AllocateMessageQueueAveragely;
import org.apache.rocketmq.client.exception.MQBrokerException;
import org.apache.rocketmq.client.exception.MQClientException;
import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.client.producer.SendStatus;
import org.apache.rocketmq.common.message.Message;
import org.apache.rocketmq.common.message.MessageExt;
import org.apache.rocketmq.common.protocol.heartbeat.MessageModel;
import org.apache.rocketmq.remoting.RPCHook;
import org.apache.rocketmq.remoting.exception.RemotingException;
import org.springframework.stereotype.Component;

/**
 * @author liwq
 * @description
 * @date 2021/9/8 11:00
 */
@Component
public class Consumer {

    //消费方
    public static void main(String[] args)
        throws MQClientException, MQBrokerException, RemotingException, InterruptedException {
        listen();
    }

    //消费
    public static void listen() throws MQClientException {
        // 1.实例化，指定组名
        DefaultMQPushConsumer consumer =
            new DefaultMQPushConsumer("blch-in-group", getAclRPCHook(), new AllocateMessageQueueAveragely());
        // 2.指定NameServer地址信息.
        //consumer.setNamesrvAddr("10.199.47.152:9876;10.199.47.153:9876");
        consumer.setNamesrvAddr("127.0.0.1:9876");
        // 3.订阅Topic和tag
        consumer.subscribe("jkztfk-to-blxd-blch", "in");
        //consumer.subscribe("user-password-modify", "in");
        // 4.消费模式(广播模式)
        consumer.setMessageModel(MessageModel.BROADCASTING);
        //同一组下不可重复
        consumer.setInstanceName("consumer-instance-1");
        // 5.注册回调函数，处理消息
        consumer.registerMessageListener(new MessageListenerConcurrently() {
            @Override
            public ConsumeConcurrentlyStatus consumeMessage(List<MessageExt> msgs, ConsumeConcurrentlyContext context) {
                for (MessageExt msg : msgs) {
                    System.out.println(
                        "consumeThreadUser=" + Thread.currentThread().getName() + "," + new String(msg.getBody()));
                }
                //                System.out.printf("%s Receive New Messages: %s %n",
                //                    Thread.currentThread().getName(), msgs);
                return ConsumeConcurrentlyStatus.CONSUME_SUCCESS;
            }
        });
        // 6.启动消息者
        consumer.start();
    }
    //
    //    public static void listenMessage1() throws MQClientException {
    //        // 1.实例化，指定组名
    //        DefaultMQPushConsumer consumer =
    //            new DefaultMQPushConsumer("group1", getAclRPCHook(), new AllocateMessageQueueAveragely());
    //        // 2.指定NameServer地址信息.
    //        consumer.setNamesrvAddr("10.199.47.152:9876;10.199.47.153:9876");
    //        // 3.订阅Topic和Tag
    //        consumer.subscribe("user-enterprise-basic-modify", "in");
    //        // 4.消费模式(集群模式)
    //        consumer.setMessageModel(MessageModel.CLUSTERING);
    //        //同一组下不可重复
    //        consumer.setInstanceName("consumer-instance-2");
    //        // 5.注册回调函数，处理消息
    //        consumer.registerMessageListener(new MessageListenerConcurrently() {
    //            @Override
    //            public ConsumeConcurrentlyStatus consumeMessage(List<MessageExt> msgs, ConsumeConcurrentlyContext context) {
    //                for (MessageExt msg : msgs) {
    //                    System.out.println(
    //                        "consumeThreadGroup=" + Thread.currentThread().getName() + "," + new String(msg.getBody()));
    //                }
    //                return ConsumeConcurrentlyStatus.CONSUME_SUCCESS;
    //            }
    //        });
    //        // 6.启动消息者
    //        consumer.start();
    //    }

    static RPCHook getAclRPCHook() {
        //测试
        return new AclClientRPCHook(new SessionCredentials("contract", "12345678"));
        //生产
        //return new AclClientRPCHook(new SessionCredentials("contract","02a3f0772"));
    }
}

